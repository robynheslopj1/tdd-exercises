module.exports = {
  verbose: true,
  testEnvironment: 'node',
  moduleDirectories: ['src/modules', 'node_modules'],
  setupFilesAfterEnv: ['<rootDir>/src/setupTests.js'],
  watchPathIgnorePatterns: ['<rootDir>/src/.+/__nock__/'],
}
