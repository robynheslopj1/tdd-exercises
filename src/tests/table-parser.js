

const parse = (table, {cellValueConverters = {}}={}) => {
  if (table === '') {
    return {
      header: [],
      rows: [],
    }
  }
  const formattedTableEntries = table
    .split('\n')
    .map(tableEntry => tableEntry.trim())
    .filter(tableEntry => !(tableEntry.startsWith('#') || tableEntry === ''))
    .map(tableEntry =>tableEntry  
        .split('|')
        .slice(1, -1)
        .map(value => value.trim())
    )
    
    const formatRowEntryItem = (rowEntryItem, headerItemName) => {
        const rowEntryConverter = cellValueConverters[headerItemName] || (v => v)
        return rowEntryItem === '' ? undefined : rowEntryConverter(rowEntryItem)
    
     }

  const [headerEntry, ...arrayOfRowEntries] = formattedTableEntries

  const formattedRows = arrayOfRowEntries.reduce(
    (arrayOfRowObjects, rowEntry) => {
      return [
        ...arrayOfRowObjects,
        headerEntry.reduce((rowObject, headerItemName, headerItemIndex) => {
          return { ...rowObject, [headerItemName]: formatRowEntryItem(rowEntry[headerItemIndex], headerItemName) }
        }, {}),
      ]
    },
    []
  )

  return {
    header: headerEntry,
    rows: formattedRows,
  }
}

export default parse
