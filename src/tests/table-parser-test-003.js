/*
 * Created by U. Holtel on 2017-08-01
 */
export default (describe, it, expect) => parse => {
  describe('table-parser.js', () => {
    describe('cell type conversion', () => {
      it('empty cells are type undefined', () => {
        const table = `
| id | name  | description  |
| 1  |       | description1 | 
| 2  | test2 |              | 
`
        expect(parse(table)).toEqual({
          header: ['id', 'name', 'description'],
          rows: [
            {
              id: '1',
              name: undefined,
              description: 'description1',
            },
            {
              id: '2',
              name: 'test2',
              description: undefined,
            },
          ],
        })
      })
      it('ids are of type number', () => {
        const table = `
| id | name  | description  |
| 1  | test1 | description1 |
| 2  | test2 | description2 |
`
        expect(
          parse(table, {
            cellValueConverters: {
              id: value => Number(value)
            },
          })
        ).toEqual({
          header: ['id', 'name', 'description'],
          rows: [
            {
              id: 1,
              name: 'test1',
              description: 'description1',
            },
            {
              id: 2,
              name: 'test2',
              description: 'description2',
            },
          ],
        })
      })
    })
  })
}
