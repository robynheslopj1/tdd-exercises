/*
 * Created by U. Holtel on 2017-08-01
 */
import test001 from './table-parser-test-001'
import test002 from './table-parser-test-002'
import test003 from './table-parser-test-003'
import test004 from './table-parser-test-004'
import test005 from './table-parser-test-005'
import parse from './table-parser'

describe(`test001`, () => {
  test001(describe, it, expect)(parse)
})

describe(`test002`, () => {
  test002(describe, it, expect)(parse)
})

describe(`test003`, () => {
  test003(describe, it, expect)(parse)
})

describe(`test004`, () => {
  test004(describe, it, expect)(parse)
})

describe(`test005`, () => {
  test005(describe, it, expect)(parse)
})
