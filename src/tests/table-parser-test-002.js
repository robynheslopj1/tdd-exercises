/*
 * Created by U. Holtel on 2017-08-01
 */
export default (describe, it, expect) => (parse) => {
  describe('table-parser.js', () => {
    describe('ignoring certain row formats', () => {
      it(`ignore leading white spaces`, () => {
        const table = 
`
\t  | id | name  | description  |
| 1  | test1 | description1 |
\t\t| 2  | test2 | description2 |
`
        expect(parse(table)).toEqual({
          header: ['id','name','description'],
          rows: [
            {
              id: '1',
              name: 'test1',
              description: 'description1'
            },
            {
              id: '2',
              name: 'test2',
              description: 'description2'
            }
          ],
        })
      })
      it(`ignore trailing white spaces`, () => {
        const table = 
`
| id | name  | description  |\t
| 1  | test1 | description1 |
| 2  | test2 | description2 |\t\t\t
`
        expect(parse(table)).toEqual({
          header: ['id','name','description'],
          rows: [
            {
              id: '1',
              name: 'test1',
              description: 'description1'
            },
            {
              id: '2',
              name: 'test2',
              description: 'description2'
            }
          ],
        })
      })
      it(`ignore empty rows`, () => {
        const table = 
`
| id | name  | description  |

| 1  | test1 | description1 |
| 3  | test3 | description3 |
`
        expect(parse(table)).toEqual({
          header: ['id','name','description'],
          rows: [
            {
              id: '1',
              name: 'test1',
              description: 'description1'
            },
            {
              id: '3',
              name: 'test3',
              description: 'description3'
            }
          ],
        })
      })
      it(`ignore rows prefixed with #`, () => {
        const table = 
`
| id | name  | description  |
| 1  | test1 | description1 |
#    | 2  | test2 | description2 |
| 3  | test3 | description3 |
    # | 4  | test4 | description4 |
`
        expect(parse(table)).toEqual({
          header: ['id','name','description'],
          rows: [
            {
              id: '1',
              name: 'test1',
              description: 'description1'
            },
            {
              id: '3',
              name: 'test3',
              description: 'description3'
            }
          ],
        })
      })
    })
  })
}
